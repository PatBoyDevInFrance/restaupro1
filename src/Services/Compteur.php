<?php

namespace App\Services;

class Compteur
{
    public function ajouter_vue()
    {
        
        $fichier_journalier =  '-' . date('d-m-Y');
        $compteur = 1;
        // écrir la condition si ce fichier exist
        if (file_exists($fichier_journalier)) {
            // le fichier exist on incrémente
            $compteur = file_get_contents($fichier_journalier);
            $compteur++;
        }
     
        file_put_contents($fichier_journalier, $compteur);
        // Sinon  on crée le fichier avec la valeur 1
        $compteur = 1;
        // écrir la condition si ce fichier exist
        if (file_exists($fichier_journalier)) {
            // le fichier exist on incrémente
            $compteur = file_get_contents($fichier_journalier);
            $compteur++;
        }
        file_put_contents($fichier_journalier, $compteur);
        // Sinon  on crée le fichier avec la valeur 1
    }

    public function incrementer_compteur(string $fichier): void
    {
        $compteur = 1;
        // écrir la condition si ce fichier exist
        if (file_exists($fichier)) {
            // le fichier exist on incrémente
            $compteur = (int)file_get_contents($fichier);
            $compteur++;
        }
        file_put_contents($fichier, $compteur);
        // Sinon  on crée le fichier avec la valeur 1
    }

    public function nombre_vues(): string
    {
        $fichier = dirname(__DIR__). DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'compteur';
        return file_get_contents($fichier);
    }

    public function nombre_vues_mois(int $annee, int $mois): int
    {
        $mois = str_pad($mois, 2, '0', STR_PAD_LEFT);
        $fichier = dirname(__DIR__). DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'compteur' . $annee . '-' . $mois . '-' . '*';
        $fichiers = glob($fichier);
        $total = 0;
        foreach ($fichiers as $fichier) {
            $total += (int)file_get_contents($fichier);
        }

        return $total;
    }

    public function nombre_vues_detail_mois(int $annee, int $mois): array
    {
        $mois = str_pad($mois, 2, '0', STR_PAD_LEFT);
        $fichier = dirname(__DIR__). DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'compteur' . $annee . '-' . $mois . '-' . '*';
        $fichiers = glob($fichier);
        $visites = [];
        foreach ($fichiers as $fichier) {
            $parties = explode('-', basename($fichier));
            $visites[] =
         [
             'annee' => $parties[1],
             'mois' => $parties[2],
             'jour' => $parties[3],
             'visites' => file_get_contents($fichier)
         ];
        }

        return $visites;
    }
}
