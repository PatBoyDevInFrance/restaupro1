<?php

namespace App\Controller;

use App\Services\Compteur;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(): Response
    {   
           
        $fichier_journalier =  '-' . date('d-m-Y');
        $compteur = 1;
        // écrir la condition si ce fichier exist
        if (file_exists($fichier_journalier)) {
            // le fichier exist on incrémente
            $compteur = file_get_contents($fichier_journalier);
            $compteur++;
        }
        
     
    file_put_contents($fichier_journalier, $compteur);
        // Sinon  on crée le fichier avec la valeur 1
        $compteur = 1;
        // écrir la condition si ce fichier exist
        if (file_exists($fichier_journalier)) {
            // le fichier exist on incrémente
            $compteur = file_get_contents($fichier_journalier);
            $compteur++;
        }
       
        //file_put_contents($fichier_journalier, $compteur);
        // Sinon  on crée le fichier avec la valeur 1
        
        

        return $this->render('home/index.html.twig', [
            'compteur' => $compteur,
            'compteurDate' => $fichier_journalier,
           
        ]);
    }
}
